package com.joyce.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ConfigServerController {

	@RequestMapping(value = "/hi")
	public String hi(HttpServletRequest request){
		String Real_IP = request.getParameter("X-Real_IP");
		return "Hi this is Eureka Server ! LocalAddr="+request.getLocalAddr()+", LocalPort=" + request.getLocalPort() + ", Real_IP="+Real_IP;
	}
	
	
}
